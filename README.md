# motor

The front-end for the multi-tenant version of Servo.


## Reading list

- https://www.citusdata.com/blog/2016/10/03/designing-your-saas-database-for-high-scalability/
- https://github.com/citusdata/django-multitenant
- https://www.vinta.com.br/blog/2017/multitenancy-juggling-customer-data-django/
- https://django-tenants.readthedocs.io/en/latest/


## Running it

    DBHOST=localhost DBNAME=motor DBUSER=servo DBPW='' ./manage.py runserver
